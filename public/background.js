// const imgUrl = "https://source.unsplash.com/collection/137627/daily"
// const imgUrl = "https://source.unsplash.com/collection/148642/1920x1080/daily"
const imgUrl = "https://source.unsplash.com/collection/2180676/1920x1080/daily"
const toData = url => fetch(url)
  .then(response => response.blob())
  .then(blob => new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onloadend = () => resolve(reader.result)
    reader.onerror = reject
    reader.readAsDataURL(blob)
  }))

let today = new Date().toISOString().slice(0, 10)
let lastUpdate = store.get('lastUpdate')

if(today != lastUpdate) {
  let base64image = toData(imgUrl).then(base64image => {
    store.set('lastUpdate', today)
    store.set('base64image', base64image.toString())
    document.body.style.backgroundImage = `url('${base64image}')`
  }, reason => { console.log(reason) });
} else {
  let base64image = store.get('base64image')
  document.body.style.backgroundImage = `url('${base64image}')`
}
